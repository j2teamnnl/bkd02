<table border="1" width="100%">
	<tr>
		<th>Mã Lớp</th>
		<th>Tên Lớp</th>
		<th>Số Lượng Sinh Viên</th>
	</tr>
	@foreach ($array as $each)
		<tr>
			<td>
				{{ $each->ma }}
			</td>
			<td>
				{{ $each->ten }}
			</td>
			<td>
				{{ $each->array_sinh_vien_count }}
			</td>
		</tr>
	@endforeach
</table>