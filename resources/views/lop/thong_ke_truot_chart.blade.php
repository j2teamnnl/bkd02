<div id="container" style="width:100%; height:400px;"></div>
<script src="{{ asset('js/highcharts.js') }}"></script>
<script type="text/javascript">
@php
$js_array_lop                = json_encode($array_lop);
$js_array_so_lan_truot       = json_encode($array_so_lan_truot);
$js_array_so_sinh_vien_truot = json_encode($array_so_sinh_vien_truot);
@endphp
var array_lop = {!! $js_array_lop !!};
var array_so_lan_truot = {!! $js_array_so_lan_truot !!};
var array_so_sinh_vien_truot = {!! $js_array_so_sinh_vien_truot !!};
Highcharts.chart('container', {
    chart: {
        zoomType: 'xy'
    },
    title: {
        text: 'Thống kê trượt'
    },
    xAxis: [{
        categories: array_lop,
        crosshair: true
    }],
    yAxis: [{ // Primary yAxis
    	allowDecimals: false,
        labels: {
            format: '{value} bạn',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        },
        title: {
            text: 'Số Sinh Viên Trượt',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        }
    }, { // Secondary yAxis
    	allowDecimals: false,
        title: {
            text: 'Số Lần Trượt',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        labels: {
            format: '{value} lần',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        opposite: true
    }],
    tooltip: {
        shared: true
    },
    legend: {
        layout: 'vertical',
        align: 'center',
        x: 0,
        verticalAlign: 'top',
        y: 50,
        floating: true,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || // theme
            'rgba(255,255,255,0.25)'
    },
    series: [{
        name: 'Số Sinh Viên Trượt',
        type: 'column',
        yAxis: 1,
        data: array_so_sinh_vien_truot,
        tooltip: {
            valueSuffix: ' bạn'
        }

    }, {
        name: 'Số Lần Trượt',
        type: 'spline',
        data: array_so_lan_truot,
        tooltip: {
            valueSuffix: ' lần'
        }
    }]
});
</script>