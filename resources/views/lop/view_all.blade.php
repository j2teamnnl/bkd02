<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<style type="text/css">
	body {
		font-family: DejaVu Sans;
	}
	</style>
</head>
<body>

<a href="{{ route('lop.view_insert') }}">
	Thêm
</a>
<br>
<a href="{{ route('lop.thong_ke_truot_excel') }}">
	Thống kê sinh viên trượt theo lớp theo Excel
</a>
<br>
<a href="{{ route('lop.thong_ke_truot_pdf') }}">
	Thống kê sinh viên trượt theo lớp theo PDF
</a>
<br>
<a href="{{ route('lop.thong_ke_truot_chart') }}">
	Thống kê sinh viên trượt theo lớp theo biểu đồ
</a>
<table border="1" width="100%">
	<tr>
		<th>Mã</th>
		<th>Tên</th>
		<th>Số lượng sinh viên</th>
		<th>Xem sinh viên lớp này</th>
		<th>Sửa</th>
		<th>Xoá</th>
	</tr>
	@foreach ($array_lop as $lop)
		<tr>
			<td>
				{{ $lop->ma }}
			</td>
			<td>
				{{ $lop->ten }}
			</td>
			<td>
				{{ $lop->so_luong_sinh_vien }}
			</td>
			<td>
				<a href="{{ route('lop.view_array_sinh_vien_by_lop',['ma' => $lop->ma]) }}">
					Xem sinh viên
				</a>
			</td>
			<td>
				<a href="{{ route('lop.view_update',['ma' => $lop->ma]) }}">
					Sửa
				</a>
			</td>
			<td>
				<a href="{{ route('lop.delete',['ma' => $lop->ma]) }}">
					Xoá
				</a>
			</td>
		</tr>
	@endforeach
</table>
</body>
</html>