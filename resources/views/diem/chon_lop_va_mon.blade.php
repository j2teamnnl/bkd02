<form action="{{ route('diem.xem_diem') }}" method="post">
	{{ csrf_field() }}
	Chọn lớp
	<select name="ma_lop">
		@foreach ($array_lop as $lop)
			<option value="{{ $lop->ma }}">
				{{ $lop->ten }}
			</option>
		@endforeach
	</select>
	<br>
	Chọn môn
	<select name="ma_mon">
		@foreach ($array_mon as $mon)
			<option value="{{ $mon->ma }}">
				{{ $mon->ten }}
			</option>
		@endforeach
	</select>
	<br>
	<button>Nhập điểm</button>
</form>