
<form action="{{ route('diem.xem_diem') }}" method="post">
	{{ csrf_field() }}
	Chọn lớp
	<select name="ma_lop">
		@foreach ($array_lop as $lop)
			<option value="{{ $lop->ma }}"
			@if ($ma_lop == $lop->ma)
				selected 
			@endif
			>
				{{ $lop->ten }}
			</option>
		@endforeach
	</select>
	<br>
	Chọn môn
	<select name="ma_mon">
		@foreach ($array_mon as $mon)
			<option value="{{ $mon->ma }}"
			@if ($ma_mon == $mon->ma)
				selected 
			@endif
			>
				{{ $mon->ten }}
			</option>
		@endforeach
	</select>
	<br>
	<button>Nhập điểm</button>
</form>

<form action="{{ route('diem.cap_nhat_diem') }}" method="post">
	{{ csrf_field() }}
<table>
	<caption>
		Bạn đang nhập điểm cho môn: {{ $ten_mon }}
		<input type="hidden" name="ma_mon" value="{{ $ma_mon }}">
	</caption>
	<tr>
		<th>Tên sinh viên</th>
		<th>Điểm</th>
	</tr>
	@foreach ($array_sinh_vien as $sinh_vien)
		<tr>
			<td>
				{{ $sinh_vien->ten }}
			</td>
			<td>
				<input type="number" name="array_diem[{{ $sinh_vien->ma }}]" value="{{ $array[$sinh_vien->ma] ?? '' }}">
			</td>
		</tr>
	@endforeach
</table>
<button>Cập nhật điểm</button>
</form>