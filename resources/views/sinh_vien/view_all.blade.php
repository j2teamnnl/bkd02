<h1>
	<a href="{{ route('sinh_vien.view_insert_excel') }}">
		Thêm bằng Excel
	</a>
</h1>
<a href="{{ route('sinh_vien.view_insert') }}">
	Thêm
</a>
<table border="1" width="100%">
	<caption>
		<form>
			Tìm kiếm tên
			<input type="search" name="tim_kiem_ten" value="{{ $tim_kiem_ten }}">
		</form>
	</caption>
	<tr>
		<th>Mã</th>
		<th>Họ</th>
		<th>Tên</th>
		<th>Ngày sinh</th>
		<th>Giới tính</th>
		<th>Tên lớp</th>
		<th>Sửa</th>
		<th>Xoá</th>
	</tr>
	@foreach ($array_sinh_vien as $sinh_vien)
		<tr>
			<td>
				{{ $sinh_vien->ma }}
			</td>
			<td>
				{{ $sinh_vien->ho }}
			</td>
			<td>
				{{ $sinh_vien->ten }}
			</td>
			<td>
				{{ $sinh_vien->ngay_sinh }}
			</td>
			<td>
				{{ $sinh_vien->gioi_tinh }}
			</td>
			<td>
				{{ $sinh_vien->lop->ten }}
			</td>
			<td>
				<a href="{{ route('sinh_vien.view_update',['ma' => $sinh_vien->ma]) }}">
					Sửa
				</a>
			</td>
			<td>
				<a href="{{ route('sinh_vien.delete',['ma' => $sinh_vien->ma]) }}">
					Xoá
				</a>
			</td>
		</tr>
	@endforeach
</table>
{{ $array_sinh_vien->links() }}