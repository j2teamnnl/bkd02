{{-- @php
	dd(old());
@endphp --}}
<style type="text/css">
.span_error{
	color: red;
}
.input_error{
	border: 1px solid red;
}
</style>
<form action="{{ route('sinh_vien.process_insert') }}" method="post">
	{{ csrf_field() }}
	Họ
	<input type="text" name="ho" 
	@if (!$errors->has('ho'))
	value="{{ old('ho') }}"
	@else
		class="input_error" 
	@endif
	>
	<span class="span_error">
		{{ $errors->first('ho') }}
	</span>
	<br>
	Tên
	<input type="text" name="ten">
	<br>
	Ngày sinh
	<input type="date" name="ngay_sinh">
	@if ($errors->first('ngay_sinh'))
		<span class="span_error">
			{{ $errors->first('ngay_sinh') }}
		</span>
	@endif
	<br>
	Email
	<input type="email" name="email" 
	@if (empty($errors->has('email')))
		value="{{ old('email') }}" 
	@endif>
	@if ($errors->first('email'))
		<span class="span_error">
			{{ $errors->first('email') }}
		</span>
	@endif
	<br>
	Giới tính
	<input type="radio" name="gioi_tinh" value="1" @if (old('gioi_tinh') == 1) checked @endif>Nữ
	<input type="radio" name="gioi_tinh" value="0">Nam
	<br>
	Lớp
	<select name="ma_lop">
		@foreach ($array_lop as $lop)
			<option value="{{ $lop->ma }}"
			@if (old('ma_lop') == $lop->ma)
				selected 
			@endif
			>
				{{ $lop->ten }}
			</option>
		@endforeach
	</select>
	@if ($errors->first('ma_lop'))
		{{ $errors->first('ma_lop') }}
	@endif
	<br>
	<button>Thêm</button>
</form>