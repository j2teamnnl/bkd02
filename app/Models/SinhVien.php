<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class SinhVien extends Model
{
    use Notifiable;
    protected $table = 'sinh_vien';
    public $timestamps = false;
    protected $fillable = [
    	'ho',
    	'ten',
    	'ngay_sinh',
        'email',
    	'gioi_tinh',
    	'ma_lop',
    ];
    protected $primaryKey = 'ma';
    public function lop()
    {
        return $this->belongsTo('App\Models\Lop','ma_lop');
    }
}
