<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mon extends Model
{
    protected $table = 'mon';
    public $timestamps = false;
    protected $fillable = ['ten'];
    protected $primaryKey = 'ma';
}
