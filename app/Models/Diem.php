<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use CoenJacobs\EloquentCompositePrimaryKeys\HasCompositePrimaryKey;

class Diem extends Model
{
	use HasCompositePrimaryKey;
    protected $table = 'diem';
    public $timestamps = false;
    public $incrementing = false;
    protected $fillable = [
    	'ma_sinh_vien',
    	'ma_mon',
    	'diem',
    ];
    protected $primaryKey = ['ma_sinh_vien','ma_mon'];
}
