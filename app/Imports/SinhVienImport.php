<?php

namespace App\Imports;

use App\Models\SinhVien;
use App\Models\Lop;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class SinhVienImport implements ToModel, WithHeadingRow
{

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        foreach ($row as $key => $value) {
            if (empty($value)) {
               unset($row[$key]);
            }
        }
        if(empty($row)){
            return;
        }

        $array = explode(' ', $row['ho_ten']);
        $ten   = array_pop($array);
        $ho    = implode(' ', $array);

        $ngay_sinh = $this->convertToDate($row['ngay_sinh']);
        $gioi_tinh = $this->getGioiTinh($row['gioi_tinh']);
        $ma_lop    = Lop::firstOrCreate(['ten' => $row['lop']])->ma;
        return new SinhVien([
            'ho'        => $ho,
            'ten'       => $ten,
            'ngay_sinh' => $ngay_sinh,
            'email'     => $row['email'] ?? '',
            'gioi_tinh' => $gioi_tinh,
            'ma_lop'    => $ma_lop,
        ]);
        
    }
    protected function convertToDate($ngay_sinh){
        return date_format(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($ngay_sinh),'Y-m-d');
    }
    protected function getGioiTinh($gioi_tinh){
        return ($gioi_tinh=='Nam') ? 1 : 0;
    }
}
