<?php

namespace App\Exports\Lop;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Models\Lop;

class ThongKeTruotExportExcel implements FromQuery, WithHeadings
{
    use Exportable;

    public function query()
    {
        return Lop::query()
        ->select('lop.ma','lop.ten')
        ->selectRaw('
		(
		    select count(*) 
			from diem 
			join sinh_vien as sv on sv.ma = diem.ma_sinh_vien 
			where sv.ma_lop = lop.ma and diem.diem < 5
		) as so_lan_truot,
		(
		    select count(DISTINCT diem.ma_sinh_vien) 
			from diem 
			join sinh_vien as sv on sv.ma = diem.ma_sinh_vien 
			where sv.ma_lop = lop.ma and diem.diem < 5
		) as so_sinh_vien_truot
        ')
        ->leftjoin('sinh_vien','sinh_vien.ma_lop','lop.ma')
        ->groupBy('lop.ma');
    }
    public function headings(): array
    {
        return [
            'Mã Lớp',
            'Tên Lớp',
            'Số Lần Trượt',
            'Số Sinh Viên Trượt',
        ];
    }
}
