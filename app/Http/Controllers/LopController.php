<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

use App\Models\Lop;
use App\Exports\Lop\ThongKeTruotExportExcel;
use Maatwebsite\Excel\Facades\Excel;

class LopController extends Controller
{
    
    public function view_all()
    {
    	$array_lop = Lop::withCount('array_sinh_vien')->get();

        // $array_lop = Lop::query()
        // ->selectRaw('lop.ma, lop.ten, COUNT(sinh_vien.ma_lop) as so_luong_sinh_vien')
        // ->leftJoin('sinh_vien','sinh_vien.ma_lop','lop.ma')
        // ->groupBy('lop.ma')
        // ->get();

    	return view("lop.view_all",[
    		'array_lop' => $array_lop
    	]);
    }
    public function view_insert()
    {
    	return view("lop.view_insert");
    }
    public function process_insert(Request $rq)
    {
    	// $lop = new Lop();
    	// $lop->ten = $rq->ten;
    	// $lop->save();

    	Lop::create($rq->all());

    	return redirect()->route('lop.view_all');
    }
    public function view_update($ma)
    {
    	// $lop = Lop::where('ma','=',$ma)->first();

    	$lop = Lop::find($ma);

    	return view("lop.view_update",[
    		'lop' => $lop
    	]);
    }
    public function process_update($ma, Request $rq)
    {
    	// $lop = Lop::find($ma);
    	// $lop->ten = $rq->ten;
    	// $lop->save();

    	// Lop::find($ma)->update([
    	// 	'ten' => $rq->ten
    	// ]);

    	Lop::find($ma)->update($rq->all());
    }
    public function delete($ma)
    {
    	// $lop = Lop::find($ma);
    	// $lop->delete();

    	Lop::find($ma)->delete();
    }
    public function view_array_sinh_vien_by_lop($ma)
    {
        $array_sinh_vien = Lop::find($ma)->array_sinh_vien;
        return view('view_array_sinh_vien_by_lop',compact('array_sinh_vien'));
    }
    public function view_lop_with_so_luong_sinh_vien()
    {
        $array = Lop::withCount('array_sinh_vien')->get();
        return view('lop.view_lop_with_so_luong_sinh_vien',compact('array'));
    }
    public function thong_ke_truot_excel()
    {
        $ngay_hom_nay = date('d_m_Y');
        return (new ThongKeTruotExportExcel)->download("thong_ke_truot_excel_$ngay_hom_nay.xlsx");
    }
    public function thong_ke_truot_pdf()
    {
        $ngay_hom_nay = date('d_m_Y');
        $array_lop = Lop::withCount('array_sinh_vien')->get();
        $pdf = PDF::loadView('lop.view_all', compact('array_lop'));
        return $pdf->download("lop_view_all.pdf");
    }
    public function thong_ke_truot_chart()
    {
        $array = Lop::query()
        ->select('lop.ma','lop.ten')
        ->selectRaw('
        (
            select count(*) 
            from diem 
            join sinh_vien as sv on sv.ma = diem.ma_sinh_vien 
            where sv.ma_lop = lop.ma and diem.diem < 5
        ) as so_lan_truot,
        (
            select count(DISTINCT diem.ma_sinh_vien) 
            from diem 
            join sinh_vien as sv on sv.ma = diem.ma_sinh_vien 
            where sv.ma_lop = lop.ma and diem.diem < 5
        ) as so_sinh_vien_truot
        ')
        ->leftjoin('sinh_vien','sinh_vien.ma_lop','lop.ma')
        ->groupBy('lop.ma')
        ->get();
        $array_lop =  array_column($array->toArray(), 'ten');  
        $array_so_lan_truot =  array_column($array->toArray(), 'so_lan_truot');  
        $array_so_sinh_vien_truot =  array_column($array->toArray(), 'so_sinh_vien_truot');  

        return view('lop.thong_ke_truot_chart',compact('array_lop','array_so_lan_truot','array_so_sinh_vien_truot'));
    }
}
