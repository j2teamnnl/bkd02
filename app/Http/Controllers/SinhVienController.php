<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Lop;
use App\Models\SinhVien;
use App\Http\Requests\SinhVienRequest;
use App\Notifications\NewStudent;
use Notification;
use App\Imports\SinhVienImport;
use Maatwebsite\Excel\Facades\Excel;

class SinhVienController extends Controller
{
    protected $table = 'sinh_vien';
    public function view_all(Request $rq)
    {
        $tim_kiem_ten = $rq->tim_kiem_ten; 
    	$array_sinh_vien = SinhVien::where('ten','like',"%$tim_kiem_ten%")->with('lop')->paginate(10);

    	return view("$this->table.view_all",[
            "array_sinh_vien" => $array_sinh_vien,
            "tim_kiem_ten"    => $tim_kiem_ten,
    	]);
    }
    public function view_insert()
    {
        $array_lop = Lop::get();
    	return view("$this->table.view_insert",[
            'array_lop' => $array_lop
        ]);
    }
    public function process_insert(Request $rq)
    {
    	$sinh_vien = SinhVien::create($rq->all());
        
        $sinh_vien->notify(new NewStudent($sinh_vien));

    	// return redirect()->route("$this->table.view_all");
    }
    public function view_update($ma)
    {
    	$sinh_vien = SinhVien::find($ma);
        $array_lop = Lop::get();

    	return view("$this->table.view_update",[
    		"sinh_vien" => $sinh_vien,
            "array_lop" => $array_lop,
    	]);
    }
    public function process_update($ma, SinhVienRequest $rq)
    {
    	SinhVien::find($ma)->update($rq->all());
    }
    public function delete($ma)
    {
    	SinhVien::find($ma)->delete();
    }
    public function view_insert_excel()
    {
        return view('sinh_vien.view_insert_excel');
    }
    public function process_insert_excel(Request $rq)
    {
        Excel::import(new SinhVienImport, $rq->file_excel);
    }

}
