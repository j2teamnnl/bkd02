<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Lop;
use App\Models\Mon;
use App\Models\SinhVien;
use App\Models\Diem;
use DB;

class DiemController
{
	public function chon_lop_va_mon()
	{
		$array_lop = Lop::get();
		$array_mon = Mon::get();
		return view('diem.chon_lop_va_mon',[
			'array_lop' => $array_lop,
			'array_mon' => $array_mon,
		]);
	}
	public function xem_diem(Request $rq)
	{
		$array_lop = Lop::get();
		$array_mon = Mon::get();

		$ma_lop = $rq->ma_lop;
		$ma_mon = $rq->ma_mon;

		// $mon = Mon::where('ma',$ma_mon)->first();
		// $ten_mon = $mon->ten;
		$ten_mon = Mon::find($ma_mon)->ten;

		// sinh viên kèm điểm
		$array_sinh_vien = SinhVien::where('ma_lop',$ma_lop)->get();

		$array_diem = Diem::where('ma_lop',$ma_lop)
		->rightJoin('sinh_vien','sinh_vien.ma','diem.ma_sinh_vien')
		->where('ma_mon',$ma_mon)
		->get(['diem','sinh_vien.ma']);

		$array = [];
		foreach ($array_diem as $each) {
			$ma_sinh_vien = $each->ma;
			$diem         = $each->diem;

			$array[$ma_sinh_vien] = $diem;
		}

		return view('diem.xem_diem',[
			'array_lop'       => $array_lop,
			'array_mon'       => $array_mon,
			'array_sinh_vien' => $array_sinh_vien,
			'ma_lop'          => $ma_lop,
			'ma_mon'          => $ma_mon,
			'ten_mon'         => $ten_mon,
			'array'           => $array,
		]);
	}
	public function cap_nhat_diem(Request $rq)
	{
		$array_diem = $rq->array_diem;
		$ma_mon     = $rq->ma_mon;
		foreach ($array_diem as $ma_sinh_vien => $diem) {
			if(isset($diem)){
				Diem::updateOrCreate([
					'ma_mon'       => $ma_mon,
					'ma_sinh_vien' => $ma_sinh_vien,
				],[
					'diem' => $diem
				]);
			}
			else{
				Diem::where([
					'ma_mon'       => $ma_mon,
					'ma_sinh_vien' => $ma_sinh_vien,
				])->delete();
			}
		}
	}
}
