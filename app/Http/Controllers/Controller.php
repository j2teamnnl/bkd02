<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Lop;
use App\Models\SinhVien;
use App\Models\Mon;
use Storage;

class Controller
{
	public function welcome()
	{
		return view('welcome');
	}
	public function test()
	{
		return view('test');
	}
	public function post_test(Request $rq)
	{
		Storage::disk('public')->put('photo',$rq->file_anh);
	}
	public function show()
	{
		return view('show');
	}
}
