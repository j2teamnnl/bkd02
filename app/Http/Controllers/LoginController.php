<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GiaoVu;
use Exception;
use Session;

class LoginController
{
    public function view_login()
    {
    	return view('view_login');
    }
    public function process_login(Request $rq)
    {
    	try {
    		$giao_vu = GiaoVu::where('email', $rq->email)
	    	->where('mat_khau',$rq->mat_khau)
	    	->firstOrFail();


	    	Session::put('ma_giao_vu',$giao_vu->ma);
	    	Session::put('ten_giao_vu',$giao_vu->ten);

	    	return redirect()->route('welcome')->with('success','Đăng nhập thành công');
    	} catch (Exception $e) {
    		return redirect()->route('view_login')->with('error','Đăng nhập sai rồi');
    	}
    }
    public function logout()
    {
    	Session::flush();

    	return redirect()->route('view_login')->with('success','Đăng xuất thành công');
    }
}
