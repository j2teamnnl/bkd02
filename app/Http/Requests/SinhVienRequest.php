<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SinhVienRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ho' => 'required|max:50|string',
            'ten' => 'required|max:50|string',
            'ngay_sinh' => 'required|date',
            'email' => 'required|email|unique:sinh_vien,email',
            'gioi_tinh' => 'required|boolean',
            'dia_chi' => 'required',
            'ma_lop' => 'required|exists:lop,ma',
        ];
    }
    public function messages()
    {
        return [
            'required' => ':attribute bắt buộc phải điền',
            'date' => ':attribute bắt buộc phải là ngày chuẩn',
            'exists' => ':attribute chọn không tồn tại',
            'unique' => ':attribute đã tồn tại',
        ];
    }
    public function attributes()
    {
        return [
            'ho' => 'Họ',
            'ten' => 'Tên',
            'ngay_sinh' => 'Ngày sinh',
            'gioi_tinh' => 'Giới tính',
            'dia_chi' => 'Địa chỉ',
            'ma_lop' => 'Lớp',
            'email' => 'Email',
        ];
    }
}
