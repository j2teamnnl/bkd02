<?php

use Illuminate\Database\Seeder;

use App\Models\SinhVien;
use App\Models\Mon;
use App\Models\Diem;

class DiemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array_sinh_vien = SinhVien::inRandomOrder()->limit(1000)->select('ma')->get();
        $array_mon = Mon::inRandomOrder()->limit(15)->select('ma')->get();

        foreach ($array_sinh_vien as $sinh_vien) {
        	foreach ($array_mon as $mon) {
	        	Diem::insert([
	        		'ma_sinh_vien' => $sinh_vien->ma,
	        		'ma_mon' => $mon->ma,
	        		'diem' => rand(0,10),
	        	]);
        	}
        }
    }
}
