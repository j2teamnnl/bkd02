<?php

use Faker\Generator as Faker;

$factory->define(App\Models\SinhVien::class, function (Faker $faker) {
    return [
        'ho' => $faker->firstName,
    	'ten' => $faker->lastName,
    	'ngay_sinh' => $faker->dateTimeBetween('-30 years','-18 years'),
    	'gioi_tinh' => $faker->boolean,
        'dia_chi' => $faker->address,
    	'ma_lop' => App\Models\Lop::inRandomOrder()->value('ma'),
    ];
});
