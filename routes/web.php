<?php


Route::get('view_lop_with_so_luong_sinh_vien','LopController@view_lop_with_so_luong_sinh_vien');

Route::get('','LoginController@view_login')->name('view_login');
Route::post('process_login','LoginController@process_login')->name('process_login');

Route::group(['middleware' => 'CheckGiaoVu'],function(){
	Route::get('welcome','Controller@welcome')->name('welcome');
	Route::get('logout','LoginController@logout')->name('logout');

	$controller = "LopController";
	Route::group(["prefix" => "lop","as" => "lop."],function() use ($controller){
		Route::get("","$controller@view_all")->name("view_all");
		Route::get("view_insert","$controller@view_insert")->name("view_insert");
		Route::post("process_insert","$controller@process_insert")->name("process_insert");
		Route::get("view_update/{ma}","$controller@view_update")->name("view_update");
		Route::post("process_update/{ma}","$controller@process_update")->name("process_update");
		Route::get("delete/{ma}","$controller@delete")->name("delete");
		
		Route::get("view_array_sinh_vien_by_lop/{ma}","$controller@view_array_sinh_vien_by_lop")->name("view_array_sinh_vien_by_lop");

		Route::get("thong_ke_truot_excel","$controller@thong_ke_truot_excel")->name("thong_ke_truot_excel");
		Route::get("thong_ke_truot_pdf","$controller@thong_ke_truot_pdf")->name("thong_ke_truot_pdf");
		Route::get("thong_ke_truot_chart","$controller@thong_ke_truot_chart")->name("thong_ke_truot_chart");
	});

	$controller = "SinhVienController";
	Route::group(["prefix" => "sinh_vien","as" => "sinh_vien."],function() use ($controller){
		Route::get("","$controller@view_all")->name("view_all");

		// form thêm bằng Excel
		Route::get("view_insert_excel","$controller@view_insert_excel")->name("view_insert_excel");
		Route::post("process_insert_excel","$controller@process_insert_excel")->name("process_insert_excel");

		Route::get("view_insert","$controller@view_insert")->name("view_insert");
		Route::post("process_insert","$controller@process_insert")->name("process_insert");
		Route::get("view_update/{ma}","$controller@view_update")->name("view_update");
		Route::post("process_update/{ma}","$controller@process_update")->name("process_update");
		Route::get("delete/{ma}","$controller@delete")->name("delete");
	});

	$controller = "DiemController";
	Route::group(["prefix" => "diem","as" => "diem."],function() use ($controller){
		Route::get("","$controller@chon_lop_va_mon")->name("chon_lop_va_mon");
		Route::post("xem_diem","$controller@xem_diem")->name("xem_diem");
		Route::post("cap_nhat_diem","$controller@cap_nhat_diem")->name("cap_nhat_diem");
	});
});

Route::get('test','Controller@test');
Route::get('show','Controller@show');
Route::post('post_test','Controller@post_test')->name('post_test');